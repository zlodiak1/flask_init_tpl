from flask import Flask
flask = Flask(__name__)
flask.config['SECRET_KEY'] = 'development key'

from app import routes