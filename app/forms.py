from flask_wtf import FlaskForm
from wtforms import TextField, SubmitField
from wtforms.validators import DataRequired, Email


class Form(FlaskForm):
   name = TextField('Name', validators=[DataRequired()])
   email = TextField('Email', validators=[Email()])
   submit = SubmitField('Submit')
