from app import flask
from app.forms import Form
from flask import render_template, url_for, request


@flask.route('/')
@flask.route('/form', methods=['GET', 'POST'])
def form():
    form = Form()
    if request.method == 'POST':
        print('form post')
        if form.validate_on_submit() == True:
            print('form valid')
        else:
            print('form NOT valid')
    else:
        print('form NON post')

    return render_template('form.html', form = form)


@flask.route('/list')
def list():
    return render_template('list.html')